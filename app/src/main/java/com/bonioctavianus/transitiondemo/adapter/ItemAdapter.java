package com.bonioctavianus.transitiondemo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bonioctavianus.transitiondemo.R;
import com.bonioctavianus.transitiondemo.model.Item;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private static OnItemClickListener clickListener;
    private List<Item> items;

    public ItemAdapter(List<Item> items) {
        this.items = items;
    }

    public void setClickListener(OnItemClickListener clickListener) {
        ItemAdapter.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_os, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = items.get(position);

        holder.ivItem.setImageResource(item.imageId);
        holder.tvItem.setText(item.titleId);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ImageView ivItem);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout containerItem;
        ImageView ivItem;
        TextView tvItem;

        ViewHolder(View view) {
            super(view);

            containerItem = (LinearLayout) view.findViewById(R.id.container_item);
            ivItem = (ImageView) view.findViewById(R.id.iv_item);
            tvItem = (TextView) view.findViewById(R.id.tv_item);

            containerItem.setOnClickListener(v -> {
                clickListener.onItemClick(getLayoutPosition(), ivItem);
            });
        }
    }
}
