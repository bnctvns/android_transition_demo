package com.bonioctavianus.transitiondemo.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.transition.TransitionManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bonioctavianus.transitiondemo.R;
import com.bonioctavianus.transitiondemo.model.Item;

public class DetailActivityTwo extends AppCompatActivity {

    private Item item;

    private LinearLayout rootContainer;
    private ImageView ivItem;
    private TextView tvTitle;
    private TextView tvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_two);

        item = (Item) getIntent().getSerializableExtra(ListActivity.KEY_INTENT_DETAIL);
        setTitle(item.titleId);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViews();
    }

    private void setupViews() {
        rootContainer = (LinearLayout) findViewById(R.id.root_container);
        ivItem = (ImageView) findViewById(R.id.iv_item);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvDescription = (TextView) findViewById(R.id.tv_description);

        ivItem.setImageResource(item.imageId);
        tvTitle.setText(item.titleId);
        tvDescription.setText(item.descriptionId);

        tvDescription.setVisibility(View.GONE);
        new Handler().postDelayed(() -> {
            TransitionManager.beginDelayedTransition(rootContainer);
            tvDescription.setVisibility(View.VISIBLE);
        }, 550);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
