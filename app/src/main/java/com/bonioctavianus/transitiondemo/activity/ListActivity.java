package com.bonioctavianus.transitiondemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bonioctavianus.transitiondemo.R;
import com.bonioctavianus.transitiondemo.adapter.ItemAdapter;
import com.bonioctavianus.transitiondemo.model.Item;
import com.bonioctavianus.transitiondemo.service.ServiceItem;

import java.util.List;

public class ListActivity extends AppCompatActivity {

    public static final String KEY_INTENT_DETAIL = "detail_item";

    private List<Item> items;
    private ItemAdapter itemAdapter;
    private RecyclerView rvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        setupViews();
    }

    private void setupViews() {
        rvItems = (RecyclerView) findViewById(R.id.rv_items);
        rvItems.setLayoutManager(new LinearLayoutManager(this));
        rvItems.setHasFixedSize(true);

        items = ServiceItem.getItems();
        itemAdapter = new ItemAdapter(items);
        itemAdapter.setClickListener(((position, sharedView) -> {
            startIntentItemDetail(items.get(position), sharedView);
        }));

        rvItems.setAdapter(itemAdapter);
    }

    private void startIntentItemDetail(Item item, View sharedView) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(KEY_INTENT_DETAIL, item);
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, sharedView, getString(R.string.shared_view_transition));
        startActivity(intent, optionsCompat.toBundle());
    }
}