package com.bonioctavianus.transitiondemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bonioctavianus.transitiondemo.R;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        setupViews();
    }

    private void setupViews() {
        findViewById(R.id.btn_intent_activity_one)
                .setOnClickListener(v -> {
                    startActivity(new Intent(this, ListActivity.class));
                });

        findViewById(R.id.btn_intent_activity_two)
                .setOnClickListener(v -> {
                    startActivity(new Intent(this, ListActivityTwo.class));
                });
    }
}
