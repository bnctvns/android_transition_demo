package com.bonioctavianus.transitiondemo.service;

import com.bonioctavianus.transitiondemo.R;
import com.bonioctavianus.transitiondemo.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ServiceItem {

    public static List<Item> getItems() {
        List<Item> items = new ArrayList<>();

        items.add(new Item(R.mipmap.ic_1, R.string.item_title_1, R.string.item_description));
        items.add(new Item(R.mipmap.ic_2, R.string.item_title_2, R.string.item_description));
        items.add(new Item(R.mipmap.ic_3, R.string.item_title_3, R.string.item_description));
        items.add(new Item(R.mipmap.ic_4, R.string.item_title_4, R.string.item_description));
        items.add(new Item(R.mipmap.ic_5, R.string.item_title_5, R.string.item_description));
        items.add(new Item(R.mipmap.ic_6, R.string.item_title_6, R.string.item_description));

        return items;
    }
}
