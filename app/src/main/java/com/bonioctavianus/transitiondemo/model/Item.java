package com.bonioctavianus.transitiondemo.model;

import java.io.Serializable;

public class Item implements Serializable {

    public int imageId;
    public int titleId;
    public int descriptionId;

    public Item(int imageId, int titleId, int descriptionId) {
        this.imageId = imageId;
        this.titleId = titleId;
        this.descriptionId = descriptionId;
    }
}
